# Upload Function

https://n7tb2bilki.execute-api.us-east-1.amazonaws.com/default/upload-function

### Body

Method: `POST`\
Content-Type: `application/json`

```json
{
  "name": "levi attacks beast titan",
  "categories": "anime,aot",
  "extension": "jpg",
  "image": "<image data in base64>"
}
```
