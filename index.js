// config
const bucket = "tkai-image-sharing";
const s3BaseUrl = "https://tkai-image-sharing.s3.amazonaws.com";
const tableName = "image_sharing";

const AWS = require("aws-sdk");
const s3 = new AWS.S3({ region: "us-east-1" });
const db = new AWS.DynamoDB.DocumentClient();

const random = (length) => {
  let result = "";
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

const save = (name, encodedImage, categories, extension, cb) => {
  const decodedImage = Buffer.from(encodedImage, "base64");
  const id = random(16);
  const fileName = id + "." + extension;
  const url = `${s3BaseUrl}/${fileName}`;
  const createdAt = new Date().toISOString();
  const downloadCount = 0;

  const params = {
    Body: decodedImage,
    Bucket: bucket,
    ContentEncoding: "base64",
    Key: fileName,
  };

  s3.putObject(params, (err, data) => {
    if (err) cb(err);
    else {
      const params = {
        TableName: tableName,
        Item: { id, name, url, categories, createdAt, downloadCount },
      };
      db.put(params, (err, data) => {
        if (err) cb(err);
        else cb();
      });
    }
  });
};

exports.handler = (event, context, cb) => {
  const { name, image, categories, extension } = JSON.parse(event.body);

  save(name, image, categories, extension, (err) => {
    cb(null, {
      statusCode: err ? 500 : 200,
      headers: { "content-type": "application/json" },
      body: JSON.stringify(err || { message: "OK" }),
    });
  });
};
